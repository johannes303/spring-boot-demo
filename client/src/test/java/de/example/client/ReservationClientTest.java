package de.example.client;

//import java.util.Arrays;
import java.util.Collection;

//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.github.tomakehurst.wiremock.client.WireMock;

//import org.apache.http.HttpHeaders;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.json.AutoConfigureJson;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties.StubsMode;
//import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
//import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureStubRunner(ids = {"de.example:service:+:8080"}, stubsMode=StubsMode.LOCAL)
//@AutoConfigureJson
//@AutoConfigureWireMock(port = 8080)
public class ReservationClientTest {

    // in JUnit 5 the use of autowired is not needed anymore
    @Autowired
    ReservationClient client;

    //@Autowired
    //ObjectMapper objectMapper;

    @Test
    public void shouldReturnAllReservations() throws Exception {
        /*
        String json = this.objectMapper
            .writeValueAsString(Arrays.asList(new Reservation(1L, "Hans"), new Reservation(2L, "Peter")));

        WireMock.stubFor(
            WireMock.get("/reservations")
                .willReturn(WireMock.aResponse()
                    .withStatus(200)
                    .withBody(json)
                    .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)));
        */
        Collection<Reservation> collection = client.getAllReservations();
        Assertions.assertThat(collection).isNotEmpty();
        boolean hasHans = collection.stream().filter(r -> r.getName().equalsIgnoreCase("Hans")).count() == 1;
        Assert.assertTrue(hasHans);
    }    
}