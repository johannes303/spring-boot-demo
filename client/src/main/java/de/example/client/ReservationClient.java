package de.example.client;

import java.util.Collection;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class ReservationClient {

    private final RestTemplate restTemplate;

    public ReservationClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public Collection<Reservation> getAllReservations() {
        ParameterizedTypeReference<Collection<Reservation>> ptr = 
            new ParameterizedTypeReference<Collection<Reservation>>() { };

        ResponseEntity<Collection<Reservation>> responseEntity = this.restTemplate
            .exchange("http://localhost:8080/reservations", HttpMethod.GET, null, ptr);

        return responseEntity.getBody();
    }
}