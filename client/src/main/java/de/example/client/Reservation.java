package de.example.client;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

// client side of reservation (dto)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Reservation {
    Long id;
    String name;
}