package de.example.service;

import java.util.Arrays;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import io.restassured.module.mockmvc.RestAssuredMockMvc;

@SpringBootTest
@RunWith(SpringRunner.class)
public class BaseClass {
 
    @Autowired
    private ReservationRestController controller;

    @MockBean
    private ReservationRepository repository;
    
    @Before
    public void before() {
        Mockito.when(this.repository.findAll()).thenReturn(
            Arrays.asList(new Reservation(1L, "Hans"), new Reservation(2L, "Peter")));

        RestAssuredMockMvc.standaloneSetup(this.controller);
    }
}