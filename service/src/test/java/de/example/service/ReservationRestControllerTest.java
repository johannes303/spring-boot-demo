package de.example.service;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest
@RunWith(SpringRunner.class)
public class ReservationRestControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    ReservationRepository reservationRepository;

    @Test
    public void testShouldFindAllReservations() throws Exception {
        Mockito.when(reservationRepository.findAll())
               .thenReturn(Arrays.asList(
                   new Reservation(1L, "Josh")
               ));

        mockMvc.perform(MockMvcRequestBuilders.get("/reservations"))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(MockMvcResultMatchers.jsonPath("@.[0].name").value("Josh"))
            .andExpect(MockMvcResultMatchers.jsonPath("@.[0].id").isNotEmpty());
    }
}
