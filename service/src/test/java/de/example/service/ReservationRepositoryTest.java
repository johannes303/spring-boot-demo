package de.example.service;

import java.util.Collection;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@DataJpaTest
@RunWith(SpringRunner.class)
public class ReservationRepositoryTest {

    @Autowired
    ReservationRepository repo;

    @Test
    public void shouldFindByName() {
        repo.save(new Reservation(null, "John"));

        Collection<Reservation> reservations = repo.findByName("John");
        Assertions.assertThat(reservations).isNotEmpty();

        Reservation found = reservations.iterator().next();
        Assertions.assertThat(found).isNotNull();
        Assertions.assertThat(found.getId()).isGreaterThan(0L);
    }
}
