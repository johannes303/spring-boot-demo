package de.example.service;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@DataJpaTest
@RunWith(SpringRunner.class)
public class ReservationEntityTest {
    
    @Autowired
    TestEntityManager testEntityManager;

    @Test
    public void reservationShouldPersist() {
        Reservation r = testEntityManager.persistFlushFind(new Reservation(null, "Peter"));

        Assertions.assertThat(r).isNotNull();
        Assertions.assertThat(r.getName()).isEqualToIgnoringCase("peter");
        Assertions.assertThat(r.getId()).isNotNull();
    }
}
