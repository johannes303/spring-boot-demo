package de.example.service;

import org.assertj.core.api.Assertions;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

public class ReservationServiceApplicationTests {
 
    @Test
    public void entityShouldConstruct() {
        Reservation reservation = new Reservation(1L, "Hans");

        Assert.assertNotNull(reservation);
        Assert.assertThat(reservation.getName(), Matchers.equalTo("Hans"));
        Assertions.assertThat(reservation.getId()).isGreaterThan(0L);
    }
}
